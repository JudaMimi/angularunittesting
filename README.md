# UnitDemo
is a demo app introducing Unit testing in Angular using Karma and Jasmine.\
![Alt text](/demo-min.gif?raw=true "Appdemo")
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.0.1.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Features
- test the component creation
- testing the h1 html tag in post
- testing the post image
- test logic; like method in the component & counter 
## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

