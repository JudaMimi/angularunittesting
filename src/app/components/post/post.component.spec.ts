import { DebugElement } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { PostComponent } from './post.component';

describe('PostComponent', () => {
  let component: PostComponent;
  let fixture: ComponentFixture<PostComponent>;
  let de: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostComponent);
    component = fixture.componentInstance;
    de = fixture.debugElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should have a h1 tag title', () => {
    expect(de.query(By.css('.post-box h1')).nativeElement.innerText).toBeDefined();
  });
  it('should have a post image', () => {
    const ele1 = fixture.debugElement.nativeElement.querySelector('.post-img');
    expect(ele1.src).toContain('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTmgyRKmsHgyUFvhgoN7xKSGh3zAAiRisuttA&usqp=CAU');
  });
  it('should increment the counter when click the heart button', () => {
    const actual = component.counter;
    component.like();
    expect(component.counter).toBeGreaterThan(0);
    expect(component.counter).toBe(actual + 1);
  });
});
